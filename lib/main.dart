import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  UnityWidgetController _controller;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            UnityWidget(
              onUnityViewCreated: _onUnityViewCreated,
              onUnityMessage: _onUnityMessage,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                child: Text("Change direction"),
                onPressed: _changeDirection,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onUnityViewCreated(UnityWidgetController controller) =>
      _controller = controller;

  void _onUnityMessage(UnityWidgetController controler, dynamic message) =>
      print("Received: \"${message.toString()}\" from Unity");

  void _changeDirection() =>
      _controller.postMessage("Cube", "ChangeDirection", "");
}
